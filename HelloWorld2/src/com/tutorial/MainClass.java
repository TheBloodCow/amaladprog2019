package com.tutorial;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass {
    public static void main(String[] args){
        ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
        HelloWorld object = (HelloWorld) context.getBean("helloWorld");
        System.out.println(object.getMessage());
    }
}
