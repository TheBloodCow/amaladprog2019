package com.blogg.blog;

import java.util.ArrayList;
import java.util.List;

public class BlogMockData {
    private List<aBlog> blogs;

    private static BlogMockData instance = null;
    public static BlogMockData getInstance(){
        if(instance ==null){
            instance = new BlogMockData();
        }
        return instance;
    }

    public BlogMockData(){
        blogs = new ArrayList<aBlog>();
        blogs.add(new aBlog(1,"With holiday travel coming up, and 2018 just around the corner, " +
                        "you may be already thinking about getaways for next year. Consider " +
                        "the Google Assistant your new travel buddy, ready at the drop of a hat—or a passport"));
        blogs.add(new aBlog(2,"No matter what questions you’re asking—whether about local traffic or " +
                        "a local business—your Google Assistant should be able to help. And starting " +
                        "today, it’s getting better at helping you, if you’re looking for nearby services " +
                        "like an electrician, plumber, house cleaner and more"));
        blogs.add(new aBlog(3,"Voice interaction is everywhere these days—via phones, TVs, laptops and smart home devices " +
                        "that use technology like the Google Assistant. And with the availability of maker-friendly " +
                        "offerings like Google AIY’s Voice Kit, the maker community has been getting in on the action " +
                        "and adding voice to their Internet of Things (IoT) projects."));
        blogs.add(new aBlog(4,"Looking at a landmark and not sure what it is? Interested in learning more about a movie as " +
                        "you stroll by the poster? With Google Lens and your Google Assistant, you now have a helpful " +
                        "sidekick to tell you more about what’s around you, right on your Pixel."));
        blogs.add(new aBlog(5, "Thanksgiving is just a few days away and, as always, your Google Assistant is ready to help. " +
                        "So while the turkey cooks and the family gathers, here are some questions to ask your Assistant."));
    }

    public List<aBlog> fetchBlogs(){
        return blogs;
    }

    public aBlog getBlogById(int id){
        for(aBlog b:blogs){
            if(b.getId()==id){
                return b;
            }
        }
        return null;
    }

    public List<aBlog> searchBlog(String searchTerm){
        List<aBlog> searchedBlogs = new ArrayList<aBlog>();
        for(aBlog b: blogs) {
            if(b.getMessage().toLowerCase().contains(searchTerm.toLowerCase())) {
                searchedBlogs.add(b);
            }
        }
        return searchedBlogs;
    }
    public aBlog createBlog(int id, String message) {
        aBlog newBlog = new aBlog(id, message);
        blogs.add(newBlog);
        return newBlog;
    }

    // update blog
    public aBlog updateBlog(int id, String message) {
        for(aBlog b: blogs) {
            if(b.getId() == id) {
                int blogIndex = blogs.indexOf(b);
                b.setMessage(message);
                blogs.set(blogIndex, b);
                return b;
            }
        }
        return null;
    }
    public boolean delete(int id){
        int blogIndex = -1;
        for(aBlog b: blogs) {
            if(b.getId() == id) {
                blogIndex = blogs.indexOf(b);
                continue;
            }
        }
        if(blogIndex > -1){
            blogs.remove(blogIndex);
        }
        return true;
    }
}
