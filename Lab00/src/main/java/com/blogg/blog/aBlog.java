package com.blogg.blog;

public class aBlog {
    private int id;
    private String message;

    public aBlog(){ }

    public aBlog(int id, String message){
        this.id=id;
        this.message=message;
    }

    public void setId(int id){
        this.id=id;
    }

    public void setMessage(String message){
        this.message=message;
    }

    public int getId(){
        return this.id;
    }

    public String getMessage(){
        return this.message;
    }

    @Override
    public String toString() {
        return "Blog{" +
                "id=" + id +
                ", message='" + message + '\'' +
                '}';
    }
}
